package com.sobackend.handler;

import com.sobackend.dto.Customer;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerHandler extends DBCommon{

    public CustomerHandler() throws SQLException {
    }

    Connection connection = super.getConnection();

    private List<Customer> mapCustomer(ResultSet resultSet) throws SQLException {
        List<Customer> list = new ArrayList<>();
        while (resultSet.next()) {
            Customer customer = new Customer();
            customer.setId(resultSet.getInt("CUSTOMMER_ID"));
            customer.setName(resultSet.getString("CUSTOMMER_NAME"));
            customer.setBirthdate(resultSet.getDate("BIRTH_DATE"));
            customer.setAddress(resultSet.getString("ADDRESS"));
            customer.setEmail(resultSet.getString("EMAIL"));
            customer.setGender(resultSet.getString("GENDER"));
            list.add(customer);
        }
        return list;
    }

    public List<Customer> getAllCus() throws SQLException {
        String sql = "SELECT * FROM custommer";
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);

        return mapCustomer(resultSet);
    }

    public Customer getCusById(Integer id) throws SQLException {
        String sql = "select * from custommer where custommer.custommer_id=?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        Integer count = 1;
        preparedStatement.setInt(count, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        return mapCustomer(resultSet).get(0);
    }

    public void deleteCusById(Integer id) throws SQLException {
        String sql = "delete from custommer where custommer.custommer_id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        Integer count = 1;
        preparedStatement.setInt(count, id);
        preparedStatement.executeUpdate();
    }
}
