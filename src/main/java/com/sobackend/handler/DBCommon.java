package com.sobackend.handler;

import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@Repository
public class DBCommon {
    protected String url = "jdbc:mysql://localhost:3306/dbso";

    protected String userName = "root";

    protected String passWord = "";

    protected Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(url, userName, passWord);
        return connection;
    }

}
