package com.sobackend.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseNoData {
    private String code;
    private String status;
    private String message;

    public ResponseNoData ok() {
        ResponseNoData responseNoData = new ResponseNoData();
        responseNoData.setCode("1");
        responseNoData.setStatus("OK");
        responseNoData.setMessage("");
        return responseNoData;
    }
}
