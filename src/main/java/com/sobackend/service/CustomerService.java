package com.sobackend.service;

import com.sobackend.dto.Customer;
import com.sobackend.handler.CustomerHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerHandler customerHandler;

    public List<Customer> getAllCus() throws SQLException {
        return customerHandler.getAllCus();
    }

    public Customer getCusById(Integer id) throws SQLException {
        return customerHandler.getCusById(id);
    }

    public void deleteCusById(Integer id) throws SQLException {
        customerHandler.deleteCusById(id);
    }

}
