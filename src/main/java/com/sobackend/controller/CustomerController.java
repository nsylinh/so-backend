package com.sobackend.controller;

import com.sobackend.dto.Customer;
import com.sobackend.response.ResponseNoData;
import com.sobackend.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping(path = "customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping(path = "getAll")
    public List<Customer> getAllCus() throws SQLException {
        try {
            return customerService.getAllCus();
        } catch (Exception e){
            throw e;
        }
    }

    @GetMapping(path = "getCusById")
    public Customer getCus(@RequestParam Integer id) throws SQLException {
        try {
            return customerService.getCusById(id);
        } catch (Exception e){
            throw e;
        }
    }

    @PostMapping(path = "deleteCusById")
    public ResponseNoData deleteCusById(Integer id) throws SQLException {
        ResponseNoData responseNoData = new ResponseNoData()   ;
        try {
            customerService.deleteCusById(id);
            return responseNoData.ok();
        } catch (Exception e){
            throw e;
        }
    }
}
