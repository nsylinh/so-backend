package com.sobackend.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Customer {
    private Integer id;
    private String name;
    private Date birthdate;
    private String gender;
    private String address;
    private String email;
}
