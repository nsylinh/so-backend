package com.sobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoBackendApplication.class, args);
	}

}
